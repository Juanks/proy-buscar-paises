import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Pais } from '../interfaces/pais.interface';

@Injectable({
  providedIn: 'root'
})
export class PaisService {
  private apiUrel: string = 'https://restcountries.com/v2';

  get httpParams(){
    return new HttpParams()
    .set('fields', 'name,capital,alpha2Code,flag,population');
  }

  constructor(private http: HttpClient) { }

  buscarPais(termino: string): Observable<Pais[]>{

    const url = `${ this.apiUrel }/name/${ termino }`
    //return this.http.get(url);
    return this.http.get<Pais[]>(url, { params: this.httpParams });
    /* return this.http.get(url)
              .pipe(
                catchError( err => of([]))
              ); */
  }

  buscarCapital(pCapital: string):Observable<Pais[]>{
    const url = `${this.apiUrel}/capital/${pCapital}`
    return this.http.get<Pais[]>(url, {params: this.httpParams});
  }

  getPaisAlpha(pId: string):Observable<Pais>{
    const url = `${this.apiUrel}/alpha/${pId}`
    return this.http.get<Pais>(url);
  }

  buscarRegion(pRegion: string):Observable<Pais[]>{
    const httpParams = new HttpParams()
      .set('fields', 'name,capital,alpha2Code,flag,population');
    const url = `${this.apiUrel}/region/${pRegion}`
    return this.http.get<Pais[]>(url, { params: httpParams })
      .pipe(
        tap( console.log)
      );
  }


}
