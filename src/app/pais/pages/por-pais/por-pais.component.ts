import { Component } from '@angular/core';
import { PaisService } from '../../services/pais.service';
import { Pais } from '../../interfaces/pais.interface';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-por-pais',
  templateUrl: './por-pais.component.html',
  styles: [
    `
      li{
        cursor: pointer;
      }
    `
  ]
})
export class PorPaisComponent {
  termino: string = '';
  hayError: boolean = false;
  paises : Pais []= [];

  paisesSugeridos: Pais[]=[];

  mostrarSugerencias: boolean = false;

  constructor(private paisService: PaisService) { }

  buscar(termino: string){
    this.mostrarSugerencias = false

    this.hayError = false
    this.termino = termino;
    console.log(this.termino);
    this.paisService.buscarPais(this.termino)
        .subscribe( respv=>{
          //console.log(respv);
          this.paises = respv;
          console.log("lista; ",this.paises);
        
        }, error =>{
          console.log('Error!!!');
          console.info('INFO ERROR',error);
          this.hayError = true;
          this.paises = [];
        })
  }

  sugerencias(termino: string){
    this.hayError = false;
    this.termino = termino;

    this.mostrarSugerencias = true;
    //TODO: crear sugerencias
    console.log("abuscar ", termino);
    this.paisService.buscarPais(termino)
    .pipe(
      tap(console.log)
    )
    /*.subscribe( listaPaises =>{
       this.paisesSugeridos = listaPaises.splice(0,5)
      console.log("paises sugeridos", this.paisesSugeridos);
    }) */
     .subscribe(
       listaPaises => this.paisesSugeridos = listaPaises.splice(0,3),
       (error) => this.paisesSugeridos =[]
       );
  }

  buscarSugerido(termino : string){
    this.buscar(termino);
    
  }

}
