import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap, tap } from 'rxjs/operators';
import { PaisService } from '../../services/pais.service';
import { Pais } from '../../interfaces/pais.interface';

@Component({
  selector: 'app-ver-pais',
  templateUrl: './ver-pais.component.html',
  styles: [
  ]
})
export class VerPaisComponent implements OnInit {

  pais!: Pais ;

  constructor(
    private activarRoute: ActivatedRoute,
    private paisService: PaisService ) { }

  ngOnInit(): void {
    /* this.activarRoute.params
    .subscribe(params =>{
      console.log("params", params);
    }) */
/* 
    this.activarRoute.params
    .subscribe(({ id }) =>{
      console.log("params", id);

      this.paisService.getPaisAlpha(id)
        .subscribe( pais => {
          console.log('Pais', pais);
        });
    }); */

    /* this.activarRoute.params
      .pipe(
        switchMap( ( { id } ) => this.paisService.getPaisAlpha( id )),
        tap( resp => console.log(resp) )
      )
      .subscribe(pais =>{
        console.log(pais);
        this.pais = pais;
      }) */


      /* this.activarRoute.params
      .pipe(
        switchMap( ( parm ) => this.paisService.getPaisAlpha( parm['id'] )),
        tap( console.log )
      )
      .subscribe(pais => this.pais = pais) */

      this.activarRoute.params
      .pipe(
        switchMap( ( {id} ) => this.paisService.getPaisAlpha( id )),
        tap( console.log )
      )
      .subscribe(pais => this.pais = pais)
  }

}
