import { Component } from '@angular/core';
import { PaisService } from '../../services/pais.service';
import { Observable } from 'rxjs';
import { Pais } from '../../interfaces/pais.interface';

@Component({
  selector: 'app-por-region',
  templateUrl: './por-region.component.html',
  styles: [
    `button{
      margin-right: 5px
    }
    `
  ]
})
export class PorRegionComponent {

  regiones: string[]= ['africa', 'americas', 'asia', 'europe', 'oceania']
  regionActiva: string='';
  paisesRegion: Pais[]=[]
  constructor( private paisService:PaisService) { }

  activarRegion(pRegion:string){
    if( pRegion === this.regionActiva ){ return ;}
    this.regionActiva = pRegion;
    this.paisesRegion =[];
    console.log("pRegion", pRegion, 'region activa: ', this.regionActiva);

    

    this.paisService.buscarRegion(pRegion)
      .subscribe(resp =>{
        //console.log(resp);
        this.paisesRegion = resp;
        console.log("paises: ", this.paisesRegion);
      });
    
  }

  getClaseCSS( pRegion: string):string{
    return (pRegion === this.regionActiva) ? 
      'btn btn-primary':'btn btn-outline-primary';
  }

}
